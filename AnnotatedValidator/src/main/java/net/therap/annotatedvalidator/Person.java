package net.therap.annotatedvalidator;

/**
 * @author nourin
 * @since 11/8/16
 */
public class Person {

    private String name;
    private int age;

    public Person(String name, int age) {
        this.name = name;
        this.age = age;
    }

    @Size(max = 20, message = "Length must be {min} - {max}")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Size(min = 18, message = "Age cannot be less than {min}")
    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

}
