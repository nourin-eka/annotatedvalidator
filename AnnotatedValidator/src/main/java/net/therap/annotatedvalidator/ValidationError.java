package net.therap.annotatedvalidator;

/**
 * @author nourin
 * @since 11/8/16
 */
public class ValidationError {

    private String field;
    private String type;
    private String fieldValue;
    private String errorMessage;

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getFieldValue() {
        return fieldValue;
    }

    public void setFieldValue(String fieldValue) {
        this.fieldValue = fieldValue;
    }

    public String getField() {
        return field;
    }

    public void setField(String field) {
        this.field = field;
    }

    @Override
    public String toString() {
        return String.format("Field Name: %s Field Type: %s Field Value: %s Error Message: %s", getField(), getType(),
                getFieldValue(), getErrorMessage());
    }
}
