package net.therap.annotatedvalidator;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

/**
 * @author nourin
 * @since 11/8/16
 */
public class AnnotatedValidator {

    public static List<ValidationError> validate(Object toValidate) {

        List<ValidationError> validationErrors = new ArrayList<>();
        Class classToValidate = toValidate.getClass();
        Method[] methods = classToValidate.getMethods();

        for (Method method : methods) {

            if (method.isAnnotationPresent(Size.class)) {

                Size size = method.getAnnotation(Size.class);
                try {

                    Object returnedObj = method.invoke(toValidate);
                    if (returnedObj.getClass().equals(Integer.class)) {
                        Integer returnedValue = (Integer) returnedObj;
                        if (returnedValue < size.min() || returnedValue > size.max()) {

                            ValidationError error = new ValidationError();
                            error.setType("int");
                            error.setField(method.getName());
                            error.setFieldValue(returnedValue.toString());
                            error.setErrorMessage(errorMessage(size.message(), size.min(), size.max()));
                            validationErrors.add(error);

                        }

                    } else if (returnedObj.getClass().equals(String.class)) {

                        String returnedString = (String) returnedObj;
                        if (returnedString.length() > size.max()) {

                            ValidationError error = new ValidationError();
                            error.setType("String");
                            error.setField(method.getName());
                            error.setFieldValue(returnedString);
                            error.setErrorMessage(errorMessage(size.message(), size.min(), size.max()));
                            validationErrors.add(error);

                        }

                    }
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                } catch (IllegalArgumentException e) {
                    e.printStackTrace();
                } catch (InvocationTargetException e) {
                    e.printStackTrace();
                }

            }
        }
        return validationErrors;
    }

    private static String errorMessage(String msg, int min, int max) {

        return msg.replaceAll("\\{min\\}", String.valueOf(min))
                .replaceAll("\\{max\\}", String.valueOf(max));
    }

    public static void printErrors(List<ValidationError> errors) {

        for (ValidationError error : errors) {

            System.out.println(error);

        }
    }
}
