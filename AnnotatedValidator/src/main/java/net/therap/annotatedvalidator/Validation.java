package net.therap.annotatedvalidator;

import java.util.ArrayList;
import java.util.List;

/**
 * @author nourin
 * @since 11/8/16
 */
public class Validation {

    public static void main(String[] args) {

        List<Person> persons = new ArrayList<Person>();
        Person p1 = new Person("AB", 10);
        Person p2 = new Person("aaaaa aaaa  aaaaaaaa aaaaaaaaaa", 120);
        Person p3 = new Person("Nourin",20);
        persons.add(p1);
        persons.add(p2);
        persons.add(p3);

        for(Person p: persons){

            List<ValidationError> errors = AnnotatedValidator.validate(p);
            AnnotatedValidator.printErrors(errors);
        }

    }
}
