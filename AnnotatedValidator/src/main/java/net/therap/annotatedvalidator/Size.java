package net.therap.annotatedvalidator;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @author nourin
 * @since 11/8/16
 */

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
public @interface Size {

    int min() default 1;
    int max() default 100;
    String message() default "Length must be between {min}-{max}";

}
